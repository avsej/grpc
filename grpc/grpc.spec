Name: grpc
Version: 1.16.1
Release: 1%{?dist}
Summary: Modern, open source, high-performance remote procedure call (RPC) framework
License: BSD
URL: https://www.grpc.io
Source0: https://github.com/grpc/grpc/archive/v%{version}/%{name}-%{version}.tar.gz
# For tests
Source2: https://github.com/google/googlemock/archive/release-1.7.0.tar.gz#/googlemock-1.7.0.tar.gz
Source3: https://github.com/google/googletest/archive/release-1.7.0.tar.gz#/googletest-1.7.0.tar.gz

BuildRequires: gcc-c++
BuildRequires: pkgconfig
BuildRequires: protobuf-devel
BuildRequires: protobuf-compiler
BuildRequires: openssl-devel
BuildRequires: gtest-devel
BuildRequires: c-ares-devel

# https://github.com/grpc/grpc/pull/15532
Patch0: 0001-patch-from-15532.patch
Patch1: 0002-fix-ignored-qualifiers-warning.patch

%description
gRPC is a modern open source high performance RPC framework that can run in any
environment. It can efficiently connect services in and across data centers
with pluggable support for load balancing, tracing, health checking and
authentication. It is also applicable in last mile of distributed computing to
connect devices, mobile applications and browsers to backend services.

The main usage scenarios:

* Efficiently connecting polyglot services in microservices style architecture
* Connecting mobile devices, browser clients to backend services
* Generating efficient client libraries

Core Features that make it awesome:

* Idiomatic client libraries in 10 languages
* Highly efficient on wire and with a simple service definition framework
* Bi-directional streaming with http/2 based transport
* Pluggable auth, tracing, load balancing and health checking


%package plugins
Summary: gRPC protocol buffers compiler plugins
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: protobuf-compiler

%description plugins
Plugins to the protocol buffers compiler to generate gRPC sources.

%package cli
Summary: gRPC protocol buffers cli
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: gflags
BuildRequires: gflags-devel

%description cli
Plugins to the protocol buffers compiler to generate gRPC sources.

%package devel
Summary: gRPC library development files
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Development headers and files for gRPC libraries.

%prep
%setup -q -a2 -a3
%patch0 -p1
%patch1 -p1
mv googlemock-release-1.7.0 third_party/googletest/googlemock
mv googletest-release-1.7.0 third_party/googletest/googletest

%build
%make_build

%check

%install
rm -rf %{buildroot}; mkdir %{buildroot}
make install prefix="%{buildroot}/usr"
make install-grpc-cli prefix="%{buildroot}/usr"
%ifarch x86_64
mkdir -p %{buildroot}/usr/lib64
mv %{buildroot}/usr/lib/* %{buildroot}/usr/lib64/
%endif
find %{buildroot} -type f -name '*.cc' -o -name '*.h' -o -name '*.pem' -exec chmod -x {} \;
find %{buildroot} -type f -name "*.la" -o -name '*.a' -exec rm -f {} \;

%clean
rm -rf %{buildroot}

%ldconfig_scriptlets

%files
%doc README.md
%license LICENSE
%{_libdir}/*.so.*
%{_datadir}/grpc

%files cli
%{_bindir}/grpc_cli

%files plugins
%doc README.md
%license LICENSE
%{_bindir}/grpc_*_plugin

%files devel
%{_libdir}/*.so
%attr(0644, root, root) %{_libdir}/pkgconfig/*
%{_includedir}/*

%changelog
* Fri Nov 16 2018 Sergey Avseyev <sergey.avseyev@gmail.com> - 1.16.1-1
- Initial revision
